import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class HomeProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello HomeProvider Provider');
  }

  getNivel() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getPercentual", { responseType: 'text' });
  }

  getBomba() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getBomba", { responseType: 'text' });
  }

}
