import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class ControleProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello ControleProvider Provider');

  }

  getAutomatico() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getAuto", { responseType: 'text' });
  }

  setAutomaticoJSON(ligado) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setAuto', JSON.stringify(ligado), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getBomba() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getBomba", { responseType: 'text' });
  }

  setBombaJSON(bombaLigada) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setBomba', JSON.stringify(bombaLigada), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getNivel(){
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getNivel", { responseType: 'text' });
  }

  setNivelJSON(nivel){
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setNivel', JSON.stringify(nivel), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }  
}
