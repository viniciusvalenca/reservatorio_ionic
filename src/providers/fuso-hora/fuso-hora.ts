import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class FusoHoraProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello FusoHoraProvider Provider');
  }

  setUtc(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setUtc', JSON.stringify(dados), { headers: headers })
        //this.httpClient.post(this.value.url + 'presets', { tensao1: "100", tensao2: "200", tensao3: "500" })
        .subscribe(res => {
          console.log("Resposta setUtc Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setUtc Provider: ", err);
          reject(err);
        });
    });
  }

  getUtc() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getUtc", {})
        .subscribe(res => {
          console.log("Resposta GET /getUtc: ", res);
          resolve(res);
        }, (err) => {
          console.log("Resposta ERRO /getUtc: ", err);
          reject(err);
        });
    });
  }
}
