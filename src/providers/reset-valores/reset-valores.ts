import { Injectable } from '@angular/core';
import { App, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ResetFabricaProvider } from '../reset-fabrica/reset-fabrica';
import { ProcurarReservatorioPage } from '../../pages/procurar-reservatorio/procurar-reservatorio';

@Injectable()
export class ResetValoresProvider {

  loading: any;

  unregisterBackButtonAction: any;

  constructor(public alertCtrlr: AlertController,
    public loader: LoadingController, private storage: Storage, 
    private resetFabricaProvider: ResetFabricaProvider, private app: App) {
    console.log('Hello ResetValoresProvider Provider');
  }

  confirmacaoReset() {
    let confirm = this.alertCtrlr.create({
      title: 'Tem certeza?',
      message: 'Ao confirmar, o aplicativo voltará ao modo de busca de dispositivos e a reservatório voltará ao modo de Reconfiguração conforme descrito no manual.',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Reset Abortado');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.resetFabrica();
            console.log('Reset Efetuado');
          }
        }
      ]
    });
    confirm.present();
  }

  alertaFalhaComunicacao(ip) {
    let alert = this.alertCtrlr.create({
      title: "Falha de comunicação",
      message: "Não foi possível reiniciar a reservatório de endereço: " + ip + ".",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Alerta Falha Reset');
          }
        },
      ]
    });
    alert.present();
  }

  resetFabrica() {
    this.loading = this.loader.create({
      content: "Reiniciando APP Aurora...",
    });
    this.loading.present()
      .then(() => {
        this.storage.get("urls").then(res0 => {
          console.log("getReset(): res0(urls) = ", res0);
          let urls = [];
          urls = res0;
          if (res0 != null) {
            for (let i = 0; i < urls.length; i++) {
              this.resetFabricaProvider.getReset(res0[i].url)
                .then(res => {
                  console.log("getReset(): res0[" + i + "].url = ", res0[i].url);
                  console.log("Resetar Configurações: ", res);
                }).catch(err => {
                  console.error("getReset(): res0[" + i + "].url = ", res0[i].url);
                  console.error("Erro ao resetar configurações", err);
                  // this.alertaFalhaComunicacao(res0[i].url);
                  return;
                });
            }
            this.zerarAtributos(true);//navctrl.push aqui dentro...
          }
        })
      });
  }

  zerarAtributos(reservatario) {
    if (reservatario) {
      this.storage.clear().then(res => {
        this.storage.set("urls", null).then(respUrls => {
          console.log("Storage: urls = null OK!", respUrls);
        });
        console.log("Storage apagado. -> ProcurarReservatoriosPage:: Resposta: ", res);
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
        this.app.getRootNav().setRoot(ProcurarReservatorioPage);
      })
    } else {      
      this.storage.clear();
      console.log("Storage apagado.");
    }
    this.loading.dismiss();
  }

}
