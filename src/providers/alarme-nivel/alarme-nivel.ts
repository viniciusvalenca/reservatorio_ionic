import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class AlarmeNivelProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello AlarmeNivel Provider');
  }

  getNivelAlerta() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getNivelAlerta", { responseType: 'text' });
  }

  setNivelAlerta(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setNivelAlerta', JSON.stringify(dados), { headers: headers })
        .subscribe(res => {
          console.log("Resposta setNivelAlerta Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setNivelAlerta Provider: ", err);
          reject(err);
        });
    });
  }

  setRegId(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setRegId', JSON.stringify(dados), { headers: headers })
        .subscribe(res => {
          console.log("Resposta setRegId Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setRegId Provider: ", err);
          reject(err);
        });
    });
  }
}