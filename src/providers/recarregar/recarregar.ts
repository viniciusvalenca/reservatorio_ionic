import { AjusteReservatorioProvider } from './../ajuste-reservatorio/ajuste-reservatorio';
import { App, NavController } from "ionic-angular";
import { RecarregarPage } from "../../pages/recarregar/recarregar";
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import { Storage } from '@ionic/storage';
import { FusoHoraProvider } from "../fuso-hora/fuso-hora";
import { HomeProvider } from "../home/home";
import { ControleProvider } from "../controle/controle";
import { WizardPage } from '../../pages/wizard/wizard';

@Injectable()
export class RecarregarProvider {

  canaisLuminaria: number;
  loading: any;
  resposta: any;
  temperatura: any;
  qtdePresets: number = 3;
  rootPage: any = null;
  erro: boolean = false;
  ledsPresets: Array<any> = [];

  constructor(public app: App, public navCtrl: NavController, public homeProvider: HomeProvider, 
    public controleProvider: ControleProvider, public storage: Storage, 
    public loader: LoadingController, public toast: ToastController, 
    public ajusteReservatorio: AjusteReservatorioProvider) {
    console.log('Hello RecarregarProvider Provider');
  }

  carregarAPP(nomeReservatorio) {
    console.log("recarregar.ts mdns carregarReservatorio(nomeReservatorio) = ", nomeReservatorio)
    this.carregarEstadoAutomatico();
  }

  carregarEstadoAutomatico() {
    this.loading = this.loader.create({
      content: "Carregando modo...",
    });
    this.loading.present().then(() => {
      this.controleProvider.getAutomatico()
        .subscribe(
        data => {
          console.log("GET Request Automático: ", Number(data) == 0 ? false : true);
          this.gravarAutomatico(data)
          this.loading.dismiss()
          this.carregarEstadoBomba()
        },
        err => {
          console.error("Erro ao carregar modo!", err)
        },
        () => console.log('GET Request completo'));
    })
  }

  gravarAutomatico(resp) {
    this.storage.set("ligado", Number(resp) == 0 ? false : true)
      .then(dados => {
        console.log("Storage SET Automatico: ", dados);
      })
  }

  carregarEstadoBomba() {
    this.loading = this.loader.create({
      content: "Carregando estado da bomba...",
    });
    this.loading.present().then(() => {
      this.controleProvider.getBomba()
        .subscribe(
        data => {
          console.log("GET Request Bomba: ", Number(data) == 0 ? false : true);
          this.gravarBomba(data)
          this.loading.dismiss();
          this.carregarNivelConfigurado()
        },
        err => {
          console.error("Erro ao carregar estado da bomba!", err)
        },
        () => console.log('GET Request completo'));
    })
  }

  gravarBomba(resp) {
    this.storage.set("bombaLigada", Number(resp) == 0 ? false : true)
      .then(dados => {
        console.log("GET Request bombaLigada: ", resp);
        console.log("Storage SET bombaLigada: ", dados);
      })
  }

  carregarNivelConfigurado() {
    this.loading = this.loader.create({
      content: "Carregando o nível configurado...",
    });
    this.loading.present().then(() => {
      this.controleProvider.getNivel()
        .subscribe(
        data => {
          console.log("GET Request Nivel: ", Number(data) == 0 ? false : true);
          this.gravarNivel(data)
          this.loading.dismiss();
          this.carregarLitrosMax()
        },
        err => {
          console.error("Erro ao carregar o nível configurado!", err)
        },
        () => console.log('GET Request completo'));
    })
  }

  gravarNivel(resp) {
    this.storage.set("nivel", Number(resp) == 0 ? false : true)
      .then(dados => {
        console.log("GET Request Nível: ", resp);
        console.log("Storage SET Nível: ", dados);
      })
  }

  carregarLitrosMax() {
    this.loading = this.loader.create({
      content: "Carregando a capacidade máxima do reservatorio (Lts) ...",
    });
    this.loading.present().then(() => {
      this.ajusteReservatorio.getLitro()
        .subscribe(
        data => {
          console.log("GET Request Litros: ", data);
          this.gravarLitrosMax(data)
          this.loading.dismiss();
          //this.navCtrl.setRoot(TabsPage);
          this.navCtrl.setRoot(WizardPage);
        },
        err => {
          console.error("Erro ao carregar a capacidade máxima do reservatorio!", err)
        },
        () => console.log('GET Request completo'));
    })
  }

  gravarLitrosMax(resp) {
    this.storage.set("litro", resp)
      .then(dados => {
        console.log("GET Request LitrosMax: ", resp);
        console.log("Storage SET LitrosMax: ", dados);
      })
  }
}