import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class AjusteReservatorioProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello AjusteReservatorioProvider Provider');
  }

  getLitro() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getLitros", { responseType: 'text' });
  }

  setLitroJSON(litro) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setLitros', JSON.stringify(litro), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
