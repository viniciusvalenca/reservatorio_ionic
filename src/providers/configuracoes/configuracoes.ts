import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class ConfiguracoesProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello ConfiguracoesProvider Provider');
  }

  getVolume(){
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getVolume", { responseType: 'text' });
  }

  setVolumeJSON(nivel){
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setVolume', JSON.stringify(nivel), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
