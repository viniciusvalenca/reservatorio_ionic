
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Zeroconf } from '@ionic-native/zeroconf';
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { HomeProvider } from '../providers/home/home';
import { ProcurarReservatorioProvider } from '../providers/procurar-reservatorio/procurar-reservatorio';
import { ConfiguracoesProvider } from '../providers/configuracoes/configuracoes';
import { AlarmeNivelProvider } from '../providers/alarme-nivel/alarme-nivel';
import { FusoHoraProvider } from '../providers/fuso-hora/fuso-hora';
import { AjusteReservatorioProvider } from '../providers/ajuste-reservatorio/ajuste-reservatorio';
import { WizardProvider } from '../providers/wizard/wizard';
import { ControleProvider } from '../providers/controle/controle';
import { ResetValoresProvider } from '../providers/reset-valores/reset-valores';
import { ResetFabricaProvider } from '../providers/reset-fabrica/reset-fabrica';
import { ValuesProvider } from '../providers/values/values';
import { RecarregarProvider } from './../providers/recarregar/recarregar';
import { AutenticacaoProvider } from '../providers/autenticacao/autenticacao';

import { AlarmeNivelPageModule } from '../pages/alarme-nivel/alarme-nivel.module';
import { ConfiguracoesPageModule } from '../pages/configuracoes/configuracoes.module';
import { ProcurarReservatorioPageModule } from '../pages/procurar-reservatorio/procurar-reservatorio.module';
import { AjusteReservatorioPageModule } from '../pages/ajuste-reservatorio/ajuste-reservatorio.module';
import { FusoHoraPageModule } from '../pages/fuso-hora/fuso-hora.module';
import { WizardPageModule } from '../pages/wizard/wizard.module';
import { ControlePageModule } from '../pages/controle/controle.module';
import { MultiPickerModule } from 'ion-multi-picker';
import { IndisponivelPageModule } from '../pages/indisponivel/indisponivel.module';
import { AcessoNegadoPageModule } from '../pages/acesso-negado/acesso-negado.module';
import { RecarregarPageModule } from '../pages/recarregar/recarregar.module';
import { ResetFabricaPageModule } from '../pages/reset-fabrica/reset-fabrica.module';
import { RecuperarEndPageModule } from '../pages/recuperar-end/recuperar-end.module';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AlarmeNivelPage } from '../pages/alarme-nivel/alarme-nivel';
import { ConfiguracoesPage } from '../pages/configuracoes/configuracoes';
import { ControlePage } from '../pages/controle/controle';
import { ProcurarReservatorioPage } from '../pages/procurar-reservatorio/procurar-reservatorio';
import { AjusteReservatorioPage } from '../pages/ajuste-reservatorio/ajuste-reservatorio';
import { FusoHoraPage } from '../pages/fuso-hora/fuso-hora';
import { WizardPage } from '../pages/wizard/wizard';
import { IndisponivelPage } from '../pages/indisponivel/indisponivel';
import { AcessoNegadoPage } from '../pages/acesso-negado/acesso-negado';
import { RecarregarPage } from './../pages/recarregar/recarregar';
import { RecuperarEndPage } from '../pages/recuperar-end/recuperar-end';
import { ResetFabricaPage } from '../pages/reset-fabrica/reset-fabrica';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({ name: 'nivelReservatorio' }),
    MultiPickerModule,
    AlarmeNivelPageModule,
    ConfiguracoesPageModule,
    ProcurarReservatorioPageModule,
    AjusteReservatorioPageModule,
    FusoHoraPageModule,
    WizardPageModule,
    ControlePageModule,   
    HttpClientModule,
    IndisponivelPageModule,
    AcessoNegadoPageModule,
    RecarregarPageModule,
    ResetFabricaPageModule, 
    RecuperarEndPageModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AlarmeNivelPage,
    ConfiguracoesPage,
    ProcurarReservatorioPage,
    AjusteReservatorioPage,
    FusoHoraPage,
    WizardPage,
    ControlePage,
    IndisponivelPage,
    AcessoNegadoPage,
    RecarregarPage,
    ResetFabricaPage,
    RecuperarEndPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProcurarReservatorioProvider,
    ConfiguracoesProvider,
    AlarmeNivelProvider,
    FusoHoraProvider,
    AjusteReservatorioProvider,
    WizardProvider,
    ControleProvider,
    ValuesProvider,
    HomeProvider,
    Push,
    AutenticacaoProvider,
    Zeroconf,    
    RecarregarProvider,
    ResetValoresProvider,
    ResetFabricaProvider,
    
  ]
})
export class AppModule {}