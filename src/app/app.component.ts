import { RecuperarEndPage } from './../pages/recuperar-end/recuperar-end';
import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../pages/tabs/tabs';
import { ProcurarReservatorioPage } from '../pages/procurar-reservatorio/procurar-reservatorio';
import { AutenticacaoProvider } from '../providers/autenticacao/autenticacao';
import { ValuesProvider } from '../providers/values/values';
import { IndisponivelPage } from '../pages/indisponivel/indisponivel';
import { AcessoNegadoPage } from '../pages/acesso-negado/acesso-negado';
import { ControleProvider } from '../providers/controle/controle';
import { AjusteReservatorioProvider } from '../providers/ajuste-reservatorio/ajuste-reservatorio';
import { AlarmeNivelProvider } from '../providers/alarme-nivel/alarme-nivel';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage: any = null;
  nomesReservatorios: Array<any> = [];

  constructor(platform: Platform, private app: App, statusBar: StatusBar, private splashScreen: SplashScreen,
    private storage: Storage, private autenticacaoProvider: AutenticacaoProvider, private value: ValuesProvider) {

      platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];
        let activeView = nav.getActive();
        console.log('activeView: ', activeView);
  
        if (activeView != null) {
          if ((typeof activeView.instance.backButtonAction === 'function'))
            activeView.instance.backButtonAction();
  
          else if (nav.canGoBack()) {
            nav.pop();
          }
          else
            nav.parent.select(0); // goes to the first tab
        }
      });
  
      platform.ready().then(() => {
  
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        //this.storage.set("firstRun", 1);
  
        // _mqttService.connect({ username: 'xhqdirfe', password: 'w_wSnirTIXiq' });
        // _mqttService.connect(); 
  
        // console.log("Observe MQTT ...");
        // _mqttService////////////TESTE-MQTT
        //   .observe('aurora/temperatura/alerta')
        //   .subscribe((message: MqttMessage) => {
        //     console.log("MQTT: ", message.payload.toString());
        //   });
  
        this.storage.get('firstRun')
          .then((respfr) => {
            console.log("firstRun: ", respfr);
            if (respfr == null || respfr == 0) {
              console.log("Primeira execução");
              this.rootPage = ProcurarReservatorioPage;
              this.splashScreen.hide();
            } else {
              console.log("Segunda+ execução");
              this.carregarNomesReservatorios();
              let auth;
              this.storage.get("url")
                .then((resUrl) => {
                  this.value.setUrl(resUrl);
                  console.log("Sucesso ao resgatar o valor da url!", resUrl);
                  // let urlRes;
                  // if(resUrl.length > 0){
                  //   urlRes = resUrl[0];
                  // }else{
                  //   urlRes = resUrl;
                  // }
                  // console.log("urlRes = ", urlRes);
                  this.storage.get("urls").then(resUrls => {
                    let urls = [];
                    urls = resUrls;
                    console.log("app.components.ts urls resgatadas (Storage) = ", urls);
                    this.value.setUrls(urls);
                    let indexUrls = urls.findIndex(i => i.url === resUrl);
                    console.log("indexUrls = ", indexUrls);
                    if (indexUrls >= 0) {
                      this.autenticacaoProvider.getAutorizacaoJSON(resUrl)
                        .then(resp => {
                          console.log("Autorizacao: auth ESP (resp) =", resp);
                          auth = resp;
                          console.log("Autorizacao: auth ESP = ", auth['autorizacao']);
                          this.storage.get('auths')/////////Erro aqui. Investigar.
                            .then((resp) => {
                              let auths = [];
                              auths = resp;
                              console.log("app.component.ts Autorizações recuperadas (Storage) => ", auths);
                              console.log("auths[indexUrls].auth: " + auths[indexUrls].auth + " == " + "auth['autorizacao']: " + auth['autorizacao'] + " ?");
                              if (auths[indexUrls].auth == auth['autorizacao']) {
                                this.storage.set("auth", auths[indexUrls].auth);
                                this.carregarAPP();
                                // this.carregarPaginaInicial();
                              } else {
                                this.splashScreen.hide();
                                this.rootPage = AcessoNegadoPage;
                                console.log("Acesso negado!!!");
                              }
                            }).catch(error => {
                              console.error("Erro ao tentar recuperar a autenticacao em app.components.ts: ", error);
                            });
                        }).catch(err => {
                          console.error("ERRO getAutorizacao => ", err);
                          this.splashScreen.hide();
                          this.recuperarEnderecoIP();
                        });
                    } else {
                      console.error("Não existe a URL... indo para recuperarEnderecoIP()");
                      this.splashScreen.hide();
                      this.recuperarEnderecoIP();
                    }
                  }).catch(errUrls => {
                    console.error("FALHA ao resgatar o valor das urls!", errUrls);
                    this.splashScreen.hide();
                    this.recuperarEnderecoIP();
                  });
                }).catch(errUrl => {
                  console.error("FALHA ao resgatar o valor da url!", errUrl);
                  this.splashScreen.hide();
                  this.recuperarEnderecoIP();
                });
            }
          });
      });
    }

    recuperarEnderecoIP() {
      this.splashScreen.hide();
      this.rootPage = RecuperarEndPage;
    }

    carregarNomesReservatorios() {
      this.storage.get("nomesReservatorios")
        .then(res => {
          if (res != null) {
            console.log("nomesReservatorios = ", res);
            this.nomesReservatorios = res;
            this.storage.get("nomeLuminaria").then(res00 => {
              // console.log("app.components.ts:: res00 ", res00);           
              console.log("nomeLuminaria = ", res00);
              let index = this.nomesReservatorios.findIndex(i => i.alias === res00);
              this.nomesReservatorios[index].selecionada = true;
              // this.gravarQtdeCanaisReservatorios(this.nomesReservatorios[index].name);
              console.log("app.components.ts:: Luminaria: " + res00 + " selecionada: " + this.nomesReservatorios[index].selecionada);
            });
            console.log("carregarNomesReservatorios() (Storage) app.componets.ts : ", this.nomesReservatorios);
          } else {
            console.log("Nenhuma luminaria encontrada anteriormente. res = ", res);
          }
  
        }).catch(err => {
          console.error("ERRO ao regatar carregarNomesReservatorios()");
        })
    }

  paginaIndisponivel() {
    this.rootPage = IndisponivelPage
  }

  carregarAPP() {
    this.rootPage = TabsPage;
    this.splashScreen.hide();
  }
}