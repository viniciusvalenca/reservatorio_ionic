import { ResetValoresProvider } from './../../providers/reset-valores/reset-valores';
import { Component } from '@angular/core';
import { IonicPage, App } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reset-fabrica',
  templateUrl: 'reset-fabrica.html',
})
export class ResetFabricaPage {
 
  constructor(/*public resetValoresProvider: ResetValoresProvider*/) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetFabricaPage');
  }

  confirmacaoReset() {
    // this.resetValoresProvider.confirmacaoReset();
  } 

}