import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetFabricaPage } from './reset-fabrica';

@NgModule({
  declarations: [
    ResetFabricaPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetFabricaPage),
  ],
})
export class ResetFabricaPageModule {}
