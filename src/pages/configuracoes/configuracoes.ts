import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FusoHoraPage } from '../fuso-hora/fuso-hora';
import { AlarmeNivelPage } from '../alarme-nivel/alarme-nivel';
import { AjusteReservatorioPage } from '../ajuste-reservatorio/ajuste-reservatorio';
import { ResetFabricaPage } from '../reset-fabrica/reset-fabrica';
import { ProcurarReservatorioPage } from '../procurar-reservatorio/procurar-reservatorio';

@IonicPage()
@Component({
  selector: 'page-configuracoes',
  templateUrl: 'configuracoes.html',
})
export class ConfiguracoesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracoesPage');
  }

  abrirPaginaFusoHora() {
    this.navCtrl.push(FusoHoraPage);
  }

  abrirPaginaAlarme() {
    this.navCtrl.push(AlarmeNivelPage);
  }

  abrirPaginaAjusteReservatorio() { 
    this.navCtrl.push(AjusteReservatorioPage);
  }

  abrirPaginaRestaurar() {
    this.navCtrl.push(ResetFabricaPage);
  }

  abrirPaginaProcurarReservatorios() {
    this.navCtrl.push(ProcurarReservatorioPage);    
  }
}