import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlarmeNivelPage } from './alarme-nivel';
import { MultiPickerModule } from 'ion-multi-picker';

@NgModule({
  declarations: [
    AlarmeNivelPage,
  ],
  imports: [
    IonicPageModule.forChild(AlarmeNivelPage),
    MultiPickerModule,
  ],
})
export class AlarmeNivelPageModule {}
