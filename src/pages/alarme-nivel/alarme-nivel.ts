import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { MultiPicker } from 'ion-multi-picker';
import { Storage } from '@ionic/storage';
import { AlarmeNivelProvider } from '../../providers/alarme-nivel/alarme-nivel';
import { Platform } from 'ionic-angular/platform/platform';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@IonicPage()
@Component({
  selector: 'page-alarme-nivel',
  templateUrl: 'alarme-nivel.html',
})
export class AlarmeNivelPage {

  litro: any;
  opcoes: any;
  ligado: boolean;
  ligadoAtual: boolean;
  nivelMin: any;
  nivelMax: any;
  nivelMinAtual: any;
  nivelMaxAtual: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private storage: Storage, private push: Push,
    public loader: LoadingController, public alertCtrlr: AlertController,
    private alarmeNivelProvider: AlarmeNivelProvider, private platform: Platform) {

    platform.registerBackButtonAction(() => {
      console.log("backPressed Presets");
      this.navCtrl.popToRoot();
    }, 2);

    this.carregarConfiguracoes();
    this.preencherListaSelect();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlarmeNivelPage');
  }

  ionViewWillEnter() {
  }

  carregarConfiguracoes() {
    let loading = this.loader.create({
      content: "Carregando...",
    });

    loading.present().then(() => {

      this.storage.get("litro")
        .then(dados => {
          this.litro = Number(dados);
          if (this.nivelMax <= 0) {
            this.nivelMax = this.litro;
          }
          console.log("Storage GET this.litro: ", this.litro);
          console.log("Storage GET litro: ", dados);
          this.preencherListaSelect();
        })

      this.storage.get("alarmeLigado")
        .then(dados => {
          loading.dismiss();
          this.ligado = Boolean(dados);
          console.log("Storage GET Alarme ligado: ", this.ligado);
          this.carregarNiveis();
        })
    })
  }

  preencherListaSelect() {
    let litros = [];
    let casasDecimais;
    if ((this.litro % 100000 == 0)) {
      casasDecimais = 10000
    } else if ((this.litro % 10000 == 0)) {
      casasDecimais = 1000
    } else if ((this.litro % 1000 == 0)) {
      casasDecimais = 100
    } else if ((this.litro % 100 == 0)) {
      casasDecimais = 10
    } else if ((this.litro % 10 == 0)) {
      casasDecimais = 1
    }
    for (let i = 0; i <= this.litro; i++) {
      if ((i % casasDecimais) == 0) {
        litros.push({ text: String(i), value: String(i) })
        console.log("Adicionando...", i);
      }
    }

    console.log("litros: ", litros);

    this.opcoes = [
      {
        name: 'col1',
        options: litros,
      },
    ];

  }

  carregarNiveis() {
    this.storage.get("nivelMin")
      .then(dados => {
        this.nivelMin = String(dados);
        console.log("nivelMin: ", this.nivelMin);
      })

    this.storage.get("nivelMax")
      .then(dados => {
        this.nivelMax = String(dados);
        console.log("nivelMax: ", this.nivelMax);
      })
  }

  // carregarConfiguracaoAlarme() {
  //   this.storage.get('alarmeLigado').then((respostaLigado) => {
  //     this.ligado = respostaLigado;
  //     this.ligadoAtual = this.ligado;

  //     this.storage.get('nivelMax').then((respostaTempMax) => {
  //       this.nivelMax = respostaTempMax;
  //       this.nivelMaxAtual = this.nivelMax;
  //       console.log("Temperatura Máxima Storage => ", this.nivelMax)
  //     })
  //     this.storage.get('nivelMin').then((respostaTempMin) => {
  //       this.nivelMin = respostaTempMin;
  //       this.nivelMinAtual = this.nivelMin;
  //       console.log("Temperatura Mínima Storage => ", this.nivelMin)
  //     })
  //     console.log('O alarme está ligado? ', respostaLigado);
  //   }).catch((e) => {
  //     console.log('erro estado do Alarme: ', this.ligado);
  //     this.toast.create({ message: "Erro ao tentar carregar os valores de temperatura do alarme!", duration: 4000, position: 'bottom' }).present();
  //   });
  // }

  checarNivelMax() {
    if (this.nivelMin > this.nivelMax) {
      this.nivelMin = this.nivelMax;
      console.log("Diferença de nível detectada! nivelMin: " + this.nivelMin + " nivelMax: " + this.nivelMax)
    }
    this.salvarAlarme("Nível Máximo");
  }

  checarNivelMin() {
    if (this.nivelMax < this.nivelMin) {
      this.nivelMax = this.nivelMin;
      console.log("Diferença de nível detectada! nivelMax: " + this.nivelMax + " nivelMin: " + this.nivelMin)
    }
    this.salvarAlarme("Nível Mínimo");
  }

  ligarDesligarAlarme() {
    this.ligado = !this.ligado;
    this.storage.set('alarmeLigado', this.ligado)
      .then(resp => {
        let estado;
        if (!this.ligado) {
          estado = "Desativando ";
        } else {
          estado = "Ativando ";
        }
      });
    this.pushsetup();
  }

  pushsetup() {
    let alert;

    const options: PushOptions = {
      ios: {
        alert: true,
        badge: true,
        sound: true,
      },
      android: {
        senderID: '258788047933',
        sound: true,
        vibrate: true,
        forceShow: true,
      },
      windows: {},
    };

    const pushObject: PushObject = this.push.init(options);
    console.log("this.ligado = ", this.ligado);
    if (this.ligado) {
      pushObject.on("registration").subscribe((registration: any) => {
        console.log("Token: ->", registration.registrationId);
        this.storage.get("token").then(respGetToken => {
          if (registration.registrationId != respGetToken) {
            let regId = { regId: registration.registrationId, regIdLigado: true };
            this.alarmeNivelProvider.setRegId(regId)
              .then(respRegid => {
                this.storage.set("token", registration.registrationId)
                  .then(respTken => {
                    console.log("Token guardado! ", respRegid);
                  }).catch(erroToken => {
                    console.error("Erro ao guardar Token! ", respRegid);
                    this.paginaIndisponivel();
                  });
              }).catch(erroRegId => {
                console.error("Erro ao Enviar Token (setRegId)! ", erroRegId);
                this.paginaIndisponivel();
              });
          }
        })
      });

      pushObject.on("notification").subscribe((notification: any) => {
        console.log("Push Notification: ", notification);
        if (notification.additionalData.foreground) {
          alert = this.alertCtrlr.create({
            title: notification.label,
            message: notification.message,
            buttons: ['OK'],
          });
          alert.present();
        }
      });

      pushObject.on('error')
        .subscribe(error => {
          console.error('Erro no plugin Push ', error)
          if (String(error) == "SERVICE_NOT_AVAILABLE") {
            alert = this.alertCtrlr.create({
              title: "Habilitar o serviço de alerta PUSH",
              message: "Não foi possível habilitar o serviço de alerta via PUSH. Por favor, verifique sua conexão com a internet.",
              buttons: ['OK'],
            });
            alert.present();
          }
        });

      // this.push.hasPermission()
      //   .then((res: any) => {
      //     if (res.isEnabled) {
      //       console.log('Permissão para enviar notificaçãoes PUSH concedida!', res);
      //     } else {
      //       console.log('Permissão para enviar notificaçãoes PUSH NEGADA!', res);
      //     }
      //   });

    } else {

      pushObject.on("registration").subscribe((registration: any) => {
        console.log("Token: ->", registration.registrationId);
        this.storage.get("token").then(respGetToken => {
          // if (registration.registrationId != respGetToken) {
          let regId = { regId: registration.registrationId, regIdLigado: false };
          this.alarmeNivelProvider.setRegId(regId)
            .then(respRegid => {
              this.storage.set("token", registration.registrationId)
                .then(respTken => {
                  console.log("Token guardado! ", respRegid);
                }).catch(erroToken => {
                  console.error("Erro ao guardar Token! ", respRegid);
                  this.paginaIndisponivel();
                });
            }).catch(erroRegId => {
              console.error("Erro ao Enviar Token (setRegId)! ", erroRegId);
              this.paginaIndisponivel();
            });
          // }
        })
      });

      this.push.listChannels().then(channels => {
        console.log("Canais abertos: ", channels);
      });
      pushObject.unregister().then(res => {
        console.log("Serviço push desativado => ", res);
      }).catch(err => {
        console.error("Falha ao desativar serviço push => ", err);
        if (String(err) == "SERVICE_NOT_AVAILABLE") {
          alert = this.alertCtrlr.create({
            title: "Desabilitar o serviço de alerta PUSH",
            message: "Não foi possível desabilitar o serviço de alerta via PUSH. Por favor, verifique sua conexão com a internet.",
            buttons: ['OK'],
          });
          alert.present();
        }
      });
    }
  }

  gravarNiveisStorage() {
    this.storage.set('nivelMin', this.nivelMin)
    this.storage.set('nivelMax', this.nivelMax)
      .then(() => {
        this.toast.create({ message: "Alarme Configurado!", duration: 4000, position: 'bottom' }).present();
      }).catch((e) => {
        console.error("ERRO! ", e)
      });
  }

  salvarAlarme(dado) {

    let alertaNivel = this.alertCtrlr.create({
      title: 'Nível',
      subTitle: 'Por favor, informe os valores dos nível máximo e mínimo.',
      buttons: ['OK']
    });

    let alertaSalvar = this.alertCtrlr.create({
      title: 'ERRO!',
      subTitle: 'Erro ao tentar salvar o alarme de nível.',
      buttons: ['OK']
    });

    let loading = this.loader.create({
      content: "Salvando...",
    });

    loading.present()
      .then(() => {
        this.gravarNiveisStorage();
        let niveisJSON = { nivelMin: this.nivelMin, nivelMax: this.nivelMax }
        this.alarmeNivelProvider.setNivelAlerta(niveisJSON)
          .then(respTemp => {
            this.nivelMaxAtual = this.nivelMax;
            this.nivelMinAtual = this.nivelMin;
            console.log("Niveis enviadas => ", respTemp);
            this.toast.create({ message: dado + " alterado!", duration: 4000, position: 'bottom' }).present();
            loading.dismiss();

          }).catch(erroTemp => {
            alertaSalvar.present();
            console.error("Erro ao enviar níveis... Desligando alarme! => ", erroTemp);
            loading.dismiss();
            this.ligado = false;
            this.paginaIndisponivel();
          });
        console.log("Passou na validação 2.o nível");
      });
  }

  paginaIndisponivel() {
    this.navCtrl.setRoot(IndisponivelPage);
  }
}