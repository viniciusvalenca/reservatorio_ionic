import { Component } from '@angular/core';
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { ConfiguracoesPage } from '../configuracoes/configuracoes';
import { ControlePage } from '../controle/controle';
import { Platform } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ControlePage;
  tab3Root = ConfiguracoesPage;
  tab4Root = AboutPage;

  constructor(private platform: Platform) {
    platform.registerBackButtonAction(() => {
      console.log("backPressed 1");
    }, 2);
  }
}
