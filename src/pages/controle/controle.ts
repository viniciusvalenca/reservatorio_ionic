import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Events } from 'ionic-angular';
import { MultiPicker } from 'ion-multi-picker';
import { Storage } from '@ionic/storage';
import { ControleProvider } from '../../providers/controle/controle';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@IonicPage()
@Component({
  selector: 'page-controle',
  templateUrl: 'controle.html',
})
export class ControlePage {

  ligado: boolean = false;

  segmento: any;

  bombaLigada: boolean;

  litro: any = 5000;

  opcoes: any;

  nivel: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private storage: Storage, public events: Events,
    public loader: LoadingController, private controleProvider: ControleProvider) {
    this.carregarControles();
    this.preencherListaSelect();
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter ControlePage');
    this.storage.get("litro")
    .then(dados => {
      this.litro = dados;
      this.preencherListaSelect();
      console.log("Storage GET litro: ", dados);
    })    
  }

  carregarControles() {
    let loading = this.loader.create({
      content: "Carregando...",
    });
    loading.present().then(() => {
      this.storage.get("nivel")
        .then(dados => {
          this.nivel = dados;
          console.log("Storage GET nível: ", dados);
        })

      this.storage.get("litro")
        .then(dados => {
          this.litro = dados;
          // this.preencherListaSelect();
          console.log("Storage GET litro: ", dados);
        })

      this.storage.get("ligado")
        .then(dados => {
          this.ligado = dados;
          if (this.ligado) {
            this.segmento = "Automático"
          } else {
            this.segmento = "Manual"
          }
          console.log("Storage GET Automatico ligado: ", dados);
        })

      this.storage.get("bombaLigada")
        .then(dados => {
          this.bombaLigada = dados;
          console.log("Storage GET bombaLigada: ", dados);
          loading.dismiss();
        })
    })
  }

  preencherListaSelect() {
    let litros = [];
    let casasDecimais;
    if ((this.litro % 100000 == 0)) {
      casasDecimais = 10000
    } else if ((this.litro % 10000 == 0)) {
      casasDecimais = 1000
    } else if ((this.litro % 1000 == 0)) {
      casasDecimais = 100
    } else if ((this.litro % 100 == 0)) {
      casasDecimais = 10
    } else if ((this.litro % 10 == 0)) {
      casasDecimais = 1
    }
    for (let i = 0; i <= this.litro; i++) {
      if ((i % casasDecimais) == 0) {
        litros.push({ text: String(i), value: String(i) })
        console.log("Adicionando...", i);
      }
    }

    console.log("litros: ", litros);

    this.opcoes = [
      {
        name: 'col1',
        options: litros,
      },
    ];
  }

  gravarAutomatico() {
    this.storage.set("ligado", this.ligado)
      .then(dados => {
        console.log("Storage SET ligado: ", dados);
      })
  }

  gravarBomba() {
    this.storage.set("bombaLigada", this.bombaLigada)
      .then(dados => {
        console.log("Storage SET bombaLigada: ", dados);
      })
  }

  gravarNivel() {
    this.storage.set("nivel", this.nivel)
      .then(dados => {
        console.log("Storage SET Nível: ", dados);
      })
  }

  ligarDesligarAutomatico() {
    this.ligado = !this.ligado;
    this.gravarAutomatico();
    let texto
    if (this.ligado) {
      texto = "Automático"
    } else {
      texto = "Manual"
    }
    let loading = this.loader.create({
      content: "Ativando modo " + texto,
    });
    let valor = {
      auto: this.ligado,
    }
    loading.present()
      .then(() => {
        this.controleProvider.setAutomaticoJSON(valor)
          .then(resp => {
            console.log("Resposta setAuto: ", resp);
            loading.dismiss();
            if (!this.ligado) {
              let valor = {
                bomba: this.bombaLigada,
              }
              this.controleProvider.setBombaJSON(valor)
                .then(resp => {
                  console.log("Resposta setBomba: ", resp);                  
                }).catch(err => {
                  console.error("Erro setBomba: ", err);
                })
            }
            // this.publicarBombaLigada(this.bombaLigada);
            this.toast.create({ message: "Bomba " + texto, duration: 4000, position: 'top' }).present();
          }).catch(err => {
            console.error("Erro setAuto: ", err);
            loading.dismiss();
            this.paginaIndisponivel();
          })
      })
  }

  ligarDesligarBomba() {
    this.bombaLigada = !this.bombaLigada;
    let texto;
    let texto2;
    if (this.bombaLigada) {
      texto = "ligada"
      texto2 = "Ligando"
    } else {
      texto = "desligada"
      texto2 = "Desligando"
    }
    this.gravarBomba();
    let valor = {
      bomba: this.bombaLigada,
    }
    let loading = this.loader.create({
      content: texto2 + " Bomba...",
    });
    loading.present()
      .then(() => {
        this.controleProvider.setBombaJSON(valor)
          .then(resp => {
            loading.dismiss();
            console.log("Resposta setBomba: ", resp);
            this.toast.create({ message: "Bomba " + texto, duration: 4000, position: 'top' }).present();
            this.publicarBombaLigada(this.bombaLigada);
          }).catch(err => {
            loading.dismiss();
            console.error("Erro setBomba: ", err);
            this.paginaIndisponivel();
          })
      })
  }

  configurarNivel() {
    this.gravarNivel();
    let valor = {
      nivel: this.nivel,
    }
    let loading = this.loader.create({
      content: "Configurando nível...",
    });
    loading.present()
      .then(() => {
        this.controleProvider.setNivelJSON(valor)
          .then(resp => {
            loading.dismiss();
            console.log("Resposta setNivel: ", resp);
            this.toast.create({ message: "Nível configurado: " + this.nivel, duration: 4000, position: 'bottom' }).present();
          }).catch(err => {
            loading.dismiss();            
            console.error("Erro setNivel: ", err);
            this.paginaIndisponivel();
          })
      })
  }

  publicarBombaLigada(bombaLigada) {
    this.events.publish('bombaLigada', bombaLigada);
    console.log("Tentando mudar o valor booleano (bombaLigada) da variavel em home.ts...", bombaLigada);
    // this.loading.dismiss();
  }

  paginaIndisponivel() {
    this.navCtrl.setRoot(IndisponivelPage);
  }
}