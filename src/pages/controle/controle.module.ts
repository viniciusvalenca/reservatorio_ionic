import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ControlePage } from './controle';
import { MultiPickerModule } from 'ion-multi-picker';

@NgModule({
  declarations: [
    ControlePage,
  ],
  imports: [
    IonicPageModule.forChild(ControlePage),
    MultiPickerModule,
  ],
})
export class ControlePageModule {}
