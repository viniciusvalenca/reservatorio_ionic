import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ControleProvider } from '../../providers/controle/controle';
import { Storage } from '@ionic/storage';
import { AjusteReservatorioProvider } from '../../providers/ajuste-reservatorio/ajuste-reservatorio';
import { Platform } from 'ionic-angular/platform/platform';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@IonicPage()
@Component({
  selector: 'page-ajuste-reservatorio',
  templateUrl: 'ajuste-reservatorio.html',
})
export class AjusteReservatorioPage {

  litro: any;
  nome: any;
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private storage: Storage, private platform: Platform,
    public loader: LoadingController, private controleProvider: ControleProvider,
    private ajusteReservatorioPrvider: AjusteReservatorioProvider) {

    platform.registerBackButtonAction(() => {
      console.log("backPressed Presets");
      this.navCtrl.popToRoot();
    }, 2);

    this.carregarAjustes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjusteReservatorioPage');
  }

  carregarAjustes() {
    let loading = this.loader.create({
      content: "Carregando ajustes...",
    });
    loading.present().then(() => {
      this.storage.get("litro")
        .then(dados => {
          this.litro = dados;
          console.log("Storage GET litroVolume: ", dados);
        })
    })
    this.storage.get("nome")
      .then(dados => {
        this.nome = dados;
        console.log("Storage GET Nome: ", dados);
        loading.dismiss();
      })
  }

  gravarLitros() {
    let valor = {
      litros: Number(this.litro),
    }
    this.ajusteReservatorioPrvider.setLitroJSON(valor)
      .then(res => {
        console.log("Resposta SET Litros", res);
        this.storage.set("litro", this.litro)
          .then(dados => {
            console.log("Storage SET litro: ", dados);
            this.gravarNome();
          })
      }).catch(err => {
        console.error("Erro! ", err);
        this.loading.dismiss();
        this.paginaIndisponivel();
      })
  }

  gravarNome() {
    this.storage.set("nome", this.nome)
      .then(dados => {
        console.log("Storage SET Nome: ", dados);
        this.loading.dismiss();
      }).catch(err => {
        console.error("Erro! ", err);
        this.loading.dismiss();
        this.paginaIndisponivel();
      })
  }

  gravar() {
    this.loading = this.loader.create({
      content: "Atualizar...",
    });
    this.loading.present().then(() => {
      this.gravarLitros();
    })
  }

  paginaIndisponivel() {
    this.navCtrl.setRoot(IndisponivelPage);
  }
}
