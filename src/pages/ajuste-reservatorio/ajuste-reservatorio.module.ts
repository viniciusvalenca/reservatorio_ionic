import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AjusteReservatorioPage } from './ajuste-reservatorio';

@NgModule({
  declarations: [
    AjusteReservatorioPage,
  ],
  imports: [
    IonicPageModule.forChild(AjusteReservatorioPage),
  ],
})
export class AjusteReservatorioPageModule {}
