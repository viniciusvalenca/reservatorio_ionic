import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-indisponivel',
  templateUrl: 'indisponivel.html',
})
export class IndisponivelPage {

  titulo: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    this.storage.get("nome").then(dados => {
      this.titulo = dados;
      console.log("Storage GET litroVolume: ", dados);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndisponivelPage');
  }

}
