import { Component, Inject, forwardRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { RecarregarProvider } from '../../providers/recarregar/recarregar';
import { ResetFabricaPage } from '../reset-fabrica/reset-fabrica';

@IonicPage()
@Component({
  selector: 'page-recarregar',
  templateUrl: 'recarregar.html',
})
export class RecarregarPage {

  recarregarProvider: any;

  botaoVisivel: boolean;

  constructor(@Inject(forwardRef(() => RecarregarProvider)) recarregarProvider) { 
    this.recarregarProvider = recarregarProvider;     
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter RecarregarPage');
    this.botaoVisivel = true;
  }

  reconectar() {     
    this.botaoVisivel = false;
    this.recarregarProvider.carregarLuminaria();    
  }
}