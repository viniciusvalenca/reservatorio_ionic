import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProcurarReservatorioPage } from './procurar-reservatorio';

@NgModule({
  declarations: [
    ProcurarReservatorioPage,
  ],
  imports: [
    IonicPageModule.forChild(ProcurarReservatorioPage),
  ],
})
export class ProcurarReservatorioPageModule {}
