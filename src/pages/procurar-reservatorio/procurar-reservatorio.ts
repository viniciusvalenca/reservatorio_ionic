import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ItemSliding, Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FusoHoraProvider } from '../../providers/fuso-hora/fuso-hora';
import { Storage } from '@ionic/storage';
import { App } from "ionic-angular";
import { Zeroconf } from '@ionic-native/zeroconf';
import { ProcurarReservatorioProvider } from '../../providers/procurar-reservatorio/procurar-reservatorio';
import { ValuesProvider } from '../../providers/values/values';
import { AutenticacaoProvider } from '../../providers/autenticacao/autenticacao';
import { ControleProvider } from '../../providers/controle/controle';
import { AjusteReservatorioProvider } from '../../providers/ajuste-reservatorio/ajuste-reservatorio';
import { AlarmeNivelProvider } from '../../providers/alarme-nivel/alarme-nivel';
import { TabsPage } from '../tabs/tabs';
import { WizardPage } from '../wizard/wizard';
import { ResetFabricaProvider } from '../../providers/reset-fabrica/reset-fabrica';
import { RecarregarProvider } from '../../providers/recarregar/recarregar';
import { HomeProvider } from '../../providers/home/home';

@IonicPage()
@Component({
  selector: 'page-procurar-reservatorio',
  templateUrl: 'procurar-reservatorio.html',
})
export class ProcurarReservatorioPage extends RecarregarProvider {

  resposta: any;

  loading: any;

  num: number = 0;

  respostaDns: any;

  servicoMDNS = [];

  tentarNovamente: boolean;

  timer: any;

  dadoNomeReservatorio: any;

  dadosReservatorios: any = [];

  nomeReservatorio: any;

  reservatorios = [];

  indexNome: any;

  n: number = 0;

  modo: any;

  recover: boolean;

  cancelar: boolean;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, public zeroconf: Zeroconf, 
    public procurarReservatoriosProvider: ProcurarReservatorioProvider, 
    public toast: ToastController, public loader: LoadingController, public app: App, public events: Events,
    public alertCtrlr: AlertController, public storage: Storage, private value: ValuesProvider,
    public procurarReservatorioProvider: ProcurarReservatorioProvider, public resetFabricaProvider: ResetFabricaProvider,
    public autenticacaoProvider: AutenticacaoProvider, public homeProvider: HomeProvider, public controleProvider: ControleProvider,
    public ajusteReservatorio: AjusteReservatorioProvider, public alarmeNivelProvider: AlarmeNivelProvider) {
    super(app, navCtrl, homeProvider, controleProvider, storage, loader, toast, ajusteReservatorio);
    this.modo = this.navParams.get('param1');
    this.recover = this.navParams.get('recover');
    this.cancelar = this.navParams.get('cancelar');
    this.watchMDNS();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProcurarReservatorioPage');
  }

  ionViewWillLeave() {
    this.pararWatchMDNS();
    if (this.cancelar) {
      this.popCallback();
    }
  }

  // backButtonAction() {}

  popCallback() {
    // this.navCtrl.pop().then(() => {
    console.log("acionando callback...")
    let parametros = this.navParams.get("callback");
    parametros(true);
    // });
  }

  selecionarReservatorio(nmdns, ipmdns, slidingItem: ItemSliding) {
    // console.log("ipmdns = ", ipmdns);
    // console.log("nmdns = ", nmdns);
    slidingItem.close();
    let confirm = this.alertCtrlr.create({
      title: 'Confirmação.',
      message: 'Deseja conectar-se à reservatório: ' + nmdns + ' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Recusa conexão');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.adicionaReservatorio(nmdns, ipmdns)
            console.log('Confirma conexão');
          }
        }
      ]
    });
    confirm.present();
  }

  removerReservatorio(nmdns, ipmdns, slidingItem: ItemSliding) {
    // console.log("ipmdns = ", ipmdns);
    // console.log("nmdns = ", nmdns);
    slidingItem.close();
    let confirm = this.alertCtrlr.create({
      title: 'Confirmação.',
      message: 'Deseja realmente remover a reservatório: ' + nmdns + ' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Recusa remoção');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.removeReiniciaReservatorio(nmdns, ipmdns);
            console.log('Confirma remoção');
          }
        }
      ]
    });
    confirm.present();
  }

  alertaAcesoNegado() {
    let alert = this.alertCtrlr.create({
      title: 'Acesso Negado',
      message: 'Seu acesso a este reservatório foi negado. Por favor, reinicie o reservatório pelo botão localizado em seu painel, conforme consta no manual e tente novamente.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Acesso negado ciente');
          }
        },
      ]
    });
    alert.present();
  }

  alertaErroCoexao() {
    let alert = this.alertCtrlr.create({
      title: 'Erro de comunicação',
      message: 'Falha ao tentar conectar à este reservatório.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Falha de comunicação');
          }
        },
      ]
    });
    alert.present();
  }

  alertaErroRemocao(nome) {
    let alert = this.alertCtrlr.create({
      title: 'Erro de comunicação',
      message: "Falha ao tentar remover a reservatório " + nome + ".",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Falha de comunicação');
          }
        },
      ]
    });
    alert.present();
  }

  adicionaReservatorio(nmdns, ipmdns) {
    this.storage.get("nomesReservatorios")
      .then(res => {
        if (this.modo != 2) {
          let resposta = [];
          resposta = res;
          console.log("ProcurarReservatoriosPage selecionarReservatorio(nmdns, ipmdns) => nomesReservatorios (resposta): ", resposta)
          if (resposta != null) {
            if (resposta.length > 0) {
              this.modo = 1;
            }
          } else {
            ipmdns = ipmdns[0];
            this.modo = 0;
          }
        }
        console.log("ProcurarReservatoriosPage selecionarReservatorio(nmdns, ipmdns) => modo: ", this.modo)
        this.getMDNS(nmdns, ipmdns, this.modo);
      });
  }

  removeReiniciaReservatorio(nmdns, ipmdns) {
    let loadingRemover = this.loader.create({
      content: "Removendo o reservatório " + nmdns + "...",
    });
    console.log("nmdns = ", nmdns);
    console.log("ipmdns = ", ipmdns);
    loadingRemover.present().then(() => {
      this.storage.get("nomesReservatorios")
        .then(res00 => {
          let resposta = [];
          resposta = res00;
          console.log("removerReservatorio() => nomesReservatorios (resposta): ", resposta)
          this.indexNome = resposta.findIndex(i => i.name === nmdns);
          let ReservatorioRemovido = resposta.splice(this.indexNome, 1);
          console.log("ReservatorioRemovida = ", ReservatorioRemovido)
          console.log("Nome do reservatório (" + nmdns + ") removida do array")
          console.log("removerReservatorio() => nomesReservatorios (resposta): ", resposta)
          this.publicarNomesReservatorios(resposta);
          this.storage.set("nomesReservatorios", resposta)
            .then(resNomesReservatorios => {
              console.log("Reservatorio atualizada (Storage): ", resNomesReservatorios)
            });
          this.storage.get("auths")
            .then(res01 => {
              let resposta = [];
              resposta = res01;
              console.log("removerReservatorio() => auths  (resposta): ", resposta)
              let authRemovida = resposta.splice(this.indexNome, 1);
              console.log("authRemovida = ", authRemovida)
              console.log("Autenticação do reservatório (" + nmdns + ") removida do array")
              console.log("removerReservatorio() => auths  (resposta): ", resposta)
              this.storage.set("auths", resposta)
                .then(resAuths => {
                  console.log("Array de auths atualizado (Storage): ", resAuths);
                });
              this.storage.get("urls")
                .then(res02 => {
                  let resposta = [];
                  resposta = res02;
                  console.log("removerReservatorio() => urls  (resposta): ", resposta)
                  let urlRemovida = resposta.splice(this.indexNome, 1);
                  console.log("urlRemovida = ", urlRemovida)
                  console.log("Url do reservatório (" + nmdns + ") removida do array")
                  console.log("removerReservatorio() => urls  (resposta): ", resposta)
                  this.storage.set("urls", resposta)
                    .then(resUrls => {
                      console.log("Array de urls atualizado (Storage): ", resUrls);
                    });
                });
            });
        });
      this.resetFabricaProvider.getReset(nmdns)
        .then(resetNMDNS => {
          console.log("Reservatório " + nmdns + " removida com sucesso.")
          loadingRemover.dismiss();
          this.toast.create({ message: "Reservatório " + nmdns + " removida com sucesso.", duration: 2000, position: 'top' }).present();
        }).catch(ErroNMDNS => {
          console.error("Erro ao tentar remover Reservatorio no NMDNS (getReset()): ", ErroNMDNS);
          this.resetFabricaProvider.getReset(ipmdns)
            .then(resetIPMDNS => {
              console.log("Reservatório " + nmdns + " removida com sucesso.")
              loadingRemover.dismiss();
              this.removeReservatorioArray(nmdns);
              this.toast.create({ message: "Reservatório " + nmdns + " removida com sucesso.", duration: 2000, position: 'top' }).present();
            }).catch(ErroIPMDNS => {
              // this.alertaErroRemocao(nmdns);
              loadingRemover.dismiss();
              this.removeReservatorioArray(nmdns);
              this.toast.create({ message: "Reservatório " + nmdns + " removida com sucesso, porém, a reservatório não pode ser reiniciada.", duration: 4000, position: 'top' }).present();
              console.error("Erro ao tentar remover Reservatorio no IPMDNS (getReset()): ", ErroIPMDNS);
            });
        });
    });
  }

  removeReservatorioArray(nmdns) {
    let indexNome = this.dadosReservatorios.findIndex(i => i.name === nmdns);
    // this.dadosReservatorios.splice(indexNome, 1);
    if (this.dadosReservatorios[indexNome].status == true) {
      this.dadosReservatorios[indexNome].cadastrada = false;
    } else {
      this.dadosReservatorios.splice(indexNome, 1);
    }
  }

  watchMDNS() {
    this.n = 0;
    this.dadosReservatorios = null;
    this.dadosReservatorios = [];
    let x = 0;
    this.servicoMDNS = [];
    this.zeroconf.close();
    this.tentarNovamente = false;
    let posicao = 0;
    this.loading = this.loader.create({
      content: "Procurando Reservatórios...",
    });

    this.tentarNovamente = true;

    this.loading.present().then(() => {

      ////////////////////////////TESTAR////////////////////////////
      this.timer = setTimeout(() => {
        if (this.servicoMDNS.length <= 0 || this.servicoMDNS.length == null) {
          this.loading.dismiss();
          this.zeroconf.stop();
          this.toast.create({ message: "Nenhum reservatório Encontrado!", duration: 2000, position: 'top' }).present();
          // console.log("Tentar Novamente: (dps do zeroconf unwatch) ", this.tentarNovamente);
          console.log("Timeout: Nenhum Reservatório encontrado ");
        }
        return;
      }, 5000);
      ////////////////////////////////////////////////////////     
      this.zeroconf.watch('_http._tcp.', 'local.')
        .subscribe(result => {
          console.log("Iniciando a busca por reservatórios...")
          if (result.action == 'resolved') {
            console.log('service resolved', result.service);
            if (String(result.service.ipv4Addresses) != '0.0.0.0') {
              // this.tentarNovamente = false;
              this.servicoMDNS.push(result.service);
              // this.toast.create({ message: "Reservatório(s) Encontrada(s)!", duration: 4000, position: 'bottom' }).present();
            }
            //this.servicoMDNS += JSON.parse(JSON.stringify(result.service));
            console.log("Resposta ZEROCONF: ", result.service);
            if (this.servicoMDNS.length > 0) {
              x++;
              console.log("Vezes da iteração MDNS ZeroConf: x = ", x);
              this.checarExistencia();
              clearTimeout(this.timer);
              console.log("this.servicoMDNS = ", this.servicoMDNS);
            }
          }
        });
      console.log("loading.dismiss() depois do watch.");
      //this.servicoMDNS = JSON.parse('{"domain":"local.","type":"_http._tcp.","name":"AQUASUNDEVICE","port":80,"hostname":"aquasundevice.local.","ipv4Addresses":["192.168.0.36"],"ipv6Addresses":[],"txtRecord":{}}{"domain":"local.","type":"_http._tcp.","name":"esp8266","port":80,"hostname":"esp8266.local.","ipv4Addresses":["192.168.0.42"],"ipv6Addresses":[],"txtRecord":{}}');
    });
  }

  checarExistencia() {
    console.log("Checando existência...");
    let valores = {
      name: "",
      ipv4Addresses: "",
      cadastrada: false,
      selecionada: false,
      down: false,
    };
    this.storage.get("nomeReservatorio").then(res0 => {
      this.dadoNomeReservatorio = res0;
      this.storage.get("nomesReservatorios")
        .then(res => {
          if (res != null && res.length > 0) {

            this.dadosReservatorios = null;
            this.dadosReservatorios = [];

            let resposta = res;
            console.log("checarExistencia() nomesReservatorios (res) = ", resposta)
            console.log("this.servicoMDNS.length = ", this.servicoMDNS.length);
            for (let i = 0; i < this.servicoMDNS.length; i++) {
              console.log("this.servicoMDNS[" + i + "].name = ", this.servicoMDNS[i].name)
              let index00 = resposta.findIndex(respi => respi.name === this.servicoMDNS[i].name);
              console.log("index00 = ", index00);
              if (index00 >= 0) {
                valores.name = this.servicoMDNS[i].name;
                valores.ipv4Addresses = this.servicoMDNS[i].ipv4Addresses[0];
                valores.cadastrada = true;
              } else {
                valores.name = this.servicoMDNS[i].name;
                valores.ipv4Addresses = this.servicoMDNS[i].ipv4Addresses[0];
                valores.cadastrada = false;
              }

              this.dadosReservatorios.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, down: valores.down });

              // this.dadosReservatorios.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, status: valores.status });
              console.log("this.dadosReservatorios[" + i + "].cadastrada: ", this.dadosReservatorios[i].cadastrada);
              console.log("this.dadosReservatorios[" + i + "].selecionada: ", this.dadosReservatorios[i].selecionada);
              console.log("this.dadosReservatorios: ", this.dadosReservatorios);
            }

            for (let j = 0; j < resposta.length; j++) {
              let index03 = this.dadosReservatorios.findIndex(resp => resp.name === resposta[j].name);
              console.log("index03 = ", index03)
              if (index03 < 0) {
                valores.name = resposta[j].name;
                console.log("resposta[" + j + "].name = ", resposta[j].name);
                valores.ipv4Addresses = "0.0.0.0";
                valores.cadastrada = true;
                valores.down = true;
                this.dadosReservatorios.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, down: valores.down });
              }
            }

            console.log("Nome do reservatório: this.dadoNomeReservatorio (alias)", this.dadoNomeReservatorio);
            let index01 = resposta.findIndex(respi => respi.alias === this.dadoNomeReservatorio);
            console.log("resposta.findIndex(respi => respi.alias === this.dadoNomeReservatorio) (index01)", index01);
            if (index01 >= 0) {
              let index02 = this.dadosReservatorios.findIndex(respi => respi.name === resposta[index01].name);
              console.log("resposta.findIndex(respi => respi.alias === this.nomesReservatorios) (index02)", index02);
              if (index02 >= 0) {
                this.dadosReservatorios[index02].selecionada = true;
              }
            }
          } else {
            this.dadosReservatorios = this.servicoMDNS;
          }
          this.n++;
          console.log("n = ", this.n);
          if (this.n >= this.servicoMDNS.length) {
            this.loading.dismiss();
            this.zeroconf.stop();
          }
          // this.zeroconf.close();

        });
    });
  }

  atualizarReservatorioAdicionada(nome, recover) {
    let index = this.dadosReservatorios.findIndex(i => i.name === nome);
    if (recover) {
      for (let i = 0; i < this.dadosReservatorios.length; i++) {
        this.dadosReservatorios[i].selecionada = false;
      }
      this.dadosReservatorios[index].selecionada = true;
    }
    this.dadosReservatorios[index].cadastrada = true;

    console.log("atualizarReservatorioAdicionada(): this.dadosReservatorios = ", this.dadosReservatorios);
    console.log("atualizarReservatorioAdicionada(): this.dadoNomeReservatorio = ", this.dadoNomeReservatorio);
  }

  atualizarReservatorioselecionada(ipmdns) {
    this.storage.get("urls").then(resUrls => {
      console.log("resUrls = ", resUrls);
      let index = resUrls.findIndex(i => i.url === ipmdns);
      console.log("Index resUrls = ", index);
      this.storage.get("nomesReservatorios").then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i].selecionada = false;
        }
        res[index].selecionada = true;
        this.publicarNomesReservatorios(res);
        this.storage.set("nomeReservatorio", res[index].alias)
          .then(respNome => {
            console.log("Nome do reservatório alterado");
          })
        this.storage.set("nomesReservatorios", res)
          .then(respNomes => {
            console.log("recuperar-ends.ts nomesReservatorios alterado: ", respNomes);
          });
      });
    });
  }

  guardarNomeReservatorio(nome) {
    if (nome != null) {
      this.nomeReservatorio = nome;
      console.log("this.servicoMDNS = ", this.servicoMDNS);
      this.storage.get("nomesReservatorios")
        .then(res => {
          let nomesReservatorios = [];
          if (res != null) {
            nomesReservatorios = res;
          }
          if (nomesReservatorios.length > 0) {
            nomesReservatorios.push({ name: nome, alias: nome, status: true, selecionada: false, link: false });
          } else {
            nomesReservatorios.push({ name: nome, alias: nome, status: true, selecionada: true, link: false });
          }
          if (nomesReservatorios.length > 0) {
            this.storage.set("nomesReservatorios", nomesReservatorios)
              .then(res => {
                console.log("guardarNomesReservatorios() guardados (Sorage): ", res);
                this.publicarNomesReservatorios(nomesReservatorios);
              })
          }
        })
      this.storage.get("nomeReservatorio").then(res => {
        if (res == null) {
          this.storage.set("nomeReservatorio", nome).then(resp => {
            console.log("Nome do reservatório guardado (Storage)", nome);
          });
        }
      });
    } else {
      console.error("guardarNomeReservatorio(nome): o nome veio nulo!!!");
    }
  }

  ///////////////////UNIFICAR////////////////////////////////////////////////
  publicarNomesReservatorios(nomesReservatorios) {
    this.events.publish('nomes', nomesReservatorios);
    console.log("Tentando mudar o nome da variavel em app.components.ts e home.ts...");
    // this.loading.dismiss();
  }

  pararWatchMDNS() {
    this.zeroconf.unwatch('_http._tcp.', 'local.')
      .then(res => {
        console.log("unwatch() res: ", res);
      }).catch(err => {
        console.error("unwatch() err: ", err);
      });
    clearTimeout(this.timer);
  }

  getMDNS(nmdns, ipmdns, modo) {
    this.cancelar = false;
    let auth = new Date().toLocaleString();
    let msg;
    if (modo == 0) {
      msg = "Configurando o app Smart Tank...";
    } else if (modo == 1 || modo == 2) {
      msg = "Adicionando reservatório...";
    }
    // this.pararWatchMDNS();//NOVO! TESTAR!
    this.zeroconf.close();
    this.loading = this.loader.create({
      content: "Configurando o app Smart Tank...",
    });
    this.loading.present()
      .then(() => {
        this.storage.set("Reservatorio", nmdns);
        this.procurarReservatorioProvider.getDadosReservatorioNome(nmdns)//Tenta resolver por nome
          .then((resp) => {
            this.resposta = resp;
            console.log("GET Request Resolvendo Nome Completo ", resp);
            if (modo == 0 || modo == 2) {
              this.value.setUrl(nmdns);//Altera o valor da variável url do ESP no APP
            }
            this.autenticacaoProvider.getAutorizacaoJSON(nmdns)//Busca a existência de uma chave no ESP
              .then(respauth => {
                console.log("Autorizacao ESP DNS: resp =", respauth);
                if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave no ESP...
                  this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                    .then(res00 => {
                      // this.storage.set("url", nmdns)//Grava na memória o valor acima para futuras execuções do APP
                      //   .then(res01 => {
                      //     console.log("Sucesso ao gravar a url (Nome) values.provider: " + nmdns + "=> ", res01)
                      this.guardarNomeReservatorio(nmdns);
                      console.log("this.value.addUrl(nmdns): ", nmdns);
                      this.value.addUrl(nmdns);
                      this.value.addAuth(auth);
                      this.autenticacaoProvider.setAutorizacaoJSON(auth, nmdns)//Grava no ESP a chave de autenticação
                        .then(res02 => {
                          console.log("Sucesso ao gravar autorização no ESP! => " + res02 + ", auth = " + auth);
                          if (modo == 0) {
                            this.storage.set("auth", auth)//Grava na memória do APP a chave de autenticação
                              .then(res03 => {
                                console.log("auth gravada: ", auth)
                                this.loading.dismiss();
                                console.log("this.carregarAPP() NMDNS;")
                                this.carregarAPP(this.nomeReservatorio);
                              }).catch(err03 => {
                                console.error("Erro ao gravar a autenticação: " + auth + "=> ", err03)
                              });
                          } else if (modo == 1) {
                            this.atualizarReservatorioAdicionada(nmdns, this.recover);
                            this.loading.dismiss();
                            this.toast.create({ message: "Reservatório adicionada com sucesso!", duration: 2000, position: 'top' }).present();
                          } else if (modo == 2) {
                            this.atualizarReservatorioselecionada(nmdns)
                            this.loading.dismiss();
                            this.carregarAPP(this.nomeReservatorio);
                          }
                        }).catch(err02 => {
                          console.error("Erro ao gravar autorização no ESP!", err02);
                        })
                      // }).catch(err01 => {
                      //   console.error("Erro ao gravar a url values.provider: " + nmdns + "=> ", err01)
                      // });
                    });
                } else {
                  console.log("Acesso NEGADO!");
                  this.loading.dismiss();
                  this.alertaAcesoNegado();
                  // this.toast.create({ message: "Acesso Negado a esta reservatório!", duration: 5000, position: 'bottom' }).present();
                  //this.navCtrl.push(AcessoNegadoPage);
                }
              }).catch(erroauth => {
                console.error("Nome => Erro (POST) ao tentar autenticar...", erroauth);
                this.loading.dismiss();
                this.alertaErroCoexao();
              })
          }).catch((e) => {//Não conseguiu resolver por nome!
            this.resposta = e;
            console.error("ERRO Resolvendo Nome!", e);
            this.procurarReservatoriosProvider.getDadosReservatorioIP(ipmdns)//Tenta resolver por IP
              .then((resp) => {
                this.resposta = resp;
                console.log("GET Request Resolvendo IP Completo ", resp);
                if (modo == 0 || modo == 2) {
                  this.value.setUrl(ipmdns);//Altera o valor da variável url do ESP no APP
                }
                this.autenticacaoProvider.getAutorizacaoJSON(ipmdns)//Busca a existência de uma chave no ESP
                  .then(respauth => {
                    console.log("Autorizacao ESP IP: resp =", respauth);
                    if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave...
                      this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                        .then(res00 => {
                          // this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                          //   .then(res01 => {
                          //     console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                          this.guardarNomeReservatorio(nmdns);
                          console.log("this.value.addUrl(ipmdns): ", ipmdns);
                          this.value.addUrl(ipmdns);
                          this.value.addAuth(auth);
                          this.autenticacaoProvider.setAutorizacaoJSON(auth, ipmdns)//Grava no ESP a chave de autenticação
                            .then(res02 => {
                              console.log("Sucesso ao gravar autorização no ESP!", res02);
                              if (modo == 0) {
                                this.storage.set("auth", auth)//Grava na memória do APP a chave de autenticação
                                  .then(res03 => {
                                    console.log("auth gravada: ", auth)
                                    this.loading.dismiss();
                                    console.log("this.carregarAPP() IPMDNS;")
                                    this.carregarAPP(this.nomeReservatorio);
                                  }).catch(err03 => {
                                    console.error("Erro ao gravar a autenticação: " + auth + "=> ", err03)
                                  });
                              } else if (modo == 1) {
                                this.atualizarReservatorioAdicionada(nmdns, this.recover);
                                this.loading.dismiss();
                                this.toast.create({ message: "Luinária adicionada com sucesso!", duration: 2000, position: 'top' }).present();
                              } else if (modo == 2) {
                                this.atualizarReservatorioselecionada(ipmdns);
                                this.loading.dismiss();
                                this.carregarAPP(this.nomeReservatorio);
                              }
                            }).catch(err02 => {
                              console.error("Erro ao gravar autorização no ESP!", err02);
                            })
                          // }).catch(err01 => {
                          //   console.error("Erro ao gravar a url values.provider: " + ipmdns + "=> ", err01)
                          // });
                        })
                        .catch((e) => {
                          this.resposta = e;
                          console.error("ERRO Resolvendo IP!", e);
                          this.loading.dismiss();
                        });
                    } else {
                      console.log("Acesso NEGADO!");
                      this.loading.dismiss();
                      this.alertaAcesoNegado();
                      // this.toast.create({ message: "Acesso Negado à esta reservatório!", duration: 5000, position: 'bottom' }).present();
                      // this.navCtrl.push(AcessoNegadoPage);
                    }
                  }).catch(erroauth => {
                    console.error("IP => Erro (POST) ao tentar autenticar...", erroauth);
                    this.loading.dismiss();
                    this.alertaErroCoexao();
                  })
              }).catch((e) => {
                console.error("ERRO Resolvendo IP!", e);
                this.loading.dismiss();
                this.alertaErroCoexao();
              });
          });
      });
  }
}  