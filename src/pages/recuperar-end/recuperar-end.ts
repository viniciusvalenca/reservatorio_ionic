import { ResetValoresProvider } from './../../providers/reset-valores/reset-valores';
import { ProcurarReservatorioPage} from './../procurar-reservatorio/procurar-reservatorio';
import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController, Platform, Events } from 'ionic-angular';
import { ProcurarReservatorioProvider } from '../../providers/procurar-reservatorio/procurar-reservatorio';
import { RecarregarProvider } from '../../providers/recarregar/recarregar';
import { HomeProvider } from '../../providers/home/home';
import { ValuesProvider } from '../../providers/values/values';
import { AutenticacaoProvider } from '../../providers/autenticacao/autenticacao';
import { IndisponivelPage } from '../indisponivel/indisponivel';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FusoHoraProvider } from '../../providers/fuso-hora/fuso-hora';
import { Storage } from '@ionic/storage';
import { App } from "ionic-angular";
import { Zeroconf } from '@ionic-native/zeroconf';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecarregarPage } from '../recarregar/recarregar';
import { AcessoNegadoPage } from '../acesso-negado/acesso-negado';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ControleProvider } from '../../providers/controle/controle';
import { AjusteReservatorioProvider } from '../../providers/ajuste-reservatorio/ajuste-reservatorio';

@IonicPage()
@Component({
  selector: 'page-recuperar-end',
  templateUrl: 'recuperar-end.html',
})
export class RecuperarEndPage extends RecarregarProvider {
  autorizacao: any;
  resposta: any;
  loading: any;
  servicoMDNS: any = [];
  timer: any;
  nome: any;
  nomeReservatorio: any;
  nomesReservatorios: Array<any> = [];
  reservatorios: Array<any> = [];
  numIp: any;
  unregisterBackButtonAction: any;
  n: number = 0;
  urls: Array<any>;
  autorizacoes: Array<any>;
  nomeOutra: String;

  constructor(public app: App, public navCtrl: NavController, public zeroconf: Zeroconf,
    public toast: ToastController, public loader: LoadingController, public resetValoresProvider: ResetValoresProvider,
    public storage: Storage, public http: HttpClient, public events: Events, private screenOrientation: ScreenOrientation,
    private value: ValuesProvider, private platform: Platform, public controleProvider: ControleProvider, 
    public ajusteReservatorio: AjusteReservatorioProvider,
    public alertCtrlr: AlertController, private autenticacaoProvider: AutenticacaoProvider,
    public procurarReservatorioProvider: ProcurarReservatorioProvider,
    public homeProvider: HomeProvider, public fusoHoraProvider: FusoHoraProvider) {
    super(app, navCtrl, homeProvider, controleProvider, storage, loader, toast, ajusteReservatorio)
    //this.botaoVisivel = this.navParams.get('param');  
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
      console.info("Orientação: Retrato.");
    });  
    console.log("ionViewDidEnter RecuperarEndPage")
    this.carregarAutorizacao();
    this.carregarUrls();
    this.carregarNomeReservatorio();
    this.watchMDNS();
  }

  backButtonAction() { }

  ionViewWillEnter() { }

  montarAlerta() {
    let opcoesAlerta = {
      title: "Problema de comunicação.",
      message: "Não foi possível se conectar ao reservatório " + this.nomeReservatorio + ". Por favor, certifique que ela esteja ligada e conectada a rede Wifi e escolha uma das opções abaixo. ",
      enableBackdropDismiss: false,
      buttons: [],
    };

    if (this.nomesReservatorios.length > 0) {
      opcoesAlerta.buttons.push({
        text: 'Tentar novamente',
        handler: () => {
          console.log('Tentando novamente');
          this.watchMDNS();
        }
      },
        {
          text: 'Conectar-se a outro reservatório',
          handler: () => {
            console.log('Conectando-se a outra reservatório...');
            this.alertaSelecionarReservatorios(this.reservatorios);
          }
        },
        {
          text: 'Adicionar outra reservatório',
          handler: () => {
            console.log('Adicionar outra reservatório...');
            this.navCtrl.push(ProcurarReservatorioPage, { param1: 2, cancelar: true, callback: this.callback });
          }
        },
        {
          text: 'Reiniciar conf. de fábrica',
          handler: () => {
            console.log('Reiniciando padrões');
            this.confirmacaoReset();
          }
        },
        {
          text: 'Sair do App Aurora',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        });
    } else {
      opcoesAlerta.buttons.push({
        text: 'Tentar novamente',
        handler: () => {
          console.log('Tentando novamente');
          this.watchMDNS();
        }
      },
        {
          text: 'Adicionar outro reservatório',
          handler: () => {
            console.log('Adicionar outra reservatório...');
            this.navCtrl.push(ProcurarReservatorioPage, { param1: 2, cancelar: true, recover: true, callback: this.callback });
          }
        },
        {
          text: 'Reiniciar conf. de fábrica',
          handler: () => {
            console.log('Reiniciando padrões');
            this.confirmacaoReset();
          }
        },
        {
          text: 'Sair do App Aurora',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        });
    }
    let alert = this.alertCtrlr.create(opcoesAlerta)
    alert.present();
  }

  callback = data => {
    console.log("Callback: data = ", data)
    if (data) {
      this.montarAlerta();
    }
  };

  alertaAcesoNegado(reservatorios) {
    let opcoesAlerta = {
      title: 'Acesso Negado',
      message: 'Seu acesso a esta reservatório foi negado. Por favor, reinicie a reservatório pelo botão localizado em seu painel conforme consta no manual e em seguida, reinicie seu aplicativo Aurora.',
      enableBackdropDismiss: false,
      buttons: [],
    };
    if (reservatorios.length > 1) {
      opcoesAlerta.buttons.push({
        text: "OK",
        handler: () => {
          console.log('Ciente Acesso Negado'); this.montarAlerta();
        }
      });
    } else {
      opcoesAlerta.buttons.push({
        text: 'Reiniciar conf. de fábrica',
        handler: () => {
          console.log('Reiniciando padrões');
          this.confirmacaoReset();
        }
      });
    }
    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  alertaSelecionarReservatorios(reservatorios) {
    let opcoesAlerta = {
      title: "Selecionar Reservatório",
      message: "Por favor, selecione uma reservatório. ",
      enableBackdropDismiss: false,
      inputs: [],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelando a escolha');
            this.montarAlerta();
          }
        },
        {
          text: 'OK',
          handler: (dados) => {
            this.selecionarOutraReservatorio(dados);
          }
        }
      ]
    }

    for (let i = 0; i < reservatorios.length; i++) {
      opcoesAlerta.inputs.push({ name: reservatorios[i].alias, value: { ix: i, nome: reservatorios[i].name }, label: reservatorios[i].alias, type: 'radio' });
    }

    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  carregarAutorizacao() {
    this.storage.get("auth").then(resp => {
      this.autorizacao = resp;
      console.log("Autorização recuperada (Storage): ", resp);
    });
    this.storage.get("auths").then(resp => {
      this.autorizacoes = resp;
      console.log("recuperar-ends.ts Autorizações recuperadas (Storage): ", resp);
    });
  }

  carregarNomeReservatorio() {
    this.storage.get("nomeReservatorio").then(res => {
      this.nomeReservatorio = res;
      this.carregarNomesReservatorios();
      console.log("nomeReservatorio = ", this.nomeReservatorio)
    });
  }

  carregarNomesReservatorios() {
    this.storage.get("nomesReservatorios").then(res => {
      this.nomesReservatorios = res;
      this.montarSelectReservatorios();
      console.log("nomesReservatorios = ", this.nomesReservatorios)
    });
  }

  montarSelectReservatorios() {
    if (this.nomesReservatorios.length > 0) {
      this.reservatorios = this.nomesReservatorios;
      let index = this.reservatorios.findIndex(i => i.alias === this.nomeReservatorio);
      console.log("index = ", index);
      if (index >= 0) {
        this.reservatorios.splice(index, 1);
        this.urls.splice(index, 1);
        this.autorizacoes.splice(index, 1);
      }
      console.log("reservatorios = ", this.reservatorios);
    }
  }

  carregarUrls() {
    this.storage.get("urls").then(res => {
      this.urls = res;
      console.log("Urls carregadas = ", this.urls)
    })
  }

  atualizarReservatorioSelecionada(ipmdns) {
    this.storage.get("urls").then(resUrls => {
      console.log("resUrls = ", resUrls);
      let index = resUrls.findIndex(i => i.url === ipmdns);
      console.log("Index resUrls = ", index);
      this.storage.get("nomesReservatorios").then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i].selecionada = false;
        }
        res[index].selecionada = true;
        this.publicarNomesReservatoriosMenu(res);
        this.storage.set("nomeReservatorio", res[index].alias)
          .then(respNome => {
            console.log("Nome da reservatório alterado");
          })
        this.storage.set("nomesReservatorios", res)
          .then(respNomes => {
            console.log("recuperar-ends.ts nomesReservatorios alterado: ", respNomes);
          });
      });
    });
  }

  publicarNomesReservatoriosMenu(nomesReservatorios) {
    this.events.publish('nomes', nomesReservatorios);
    console.log("Tentando mudar o nome da variavel em app.components.ts...");
    this.loading.dismiss();
  }

  selecionarOutraReservatorio(dados) {
    console.log("Escolheu a reservatório")
    console.log("valor da seleção (index)= ", dados.ix)
    console.log("valor da seleção (nome)= ", dados.nome)
    this.nomeOutra = dados.nome;
    console.log("Nome da seleção (this.nomesReservatorios[dados.ix].name) = ", this.nomesReservatorios[dados.ix].name)
    console.log("Url da seleção (this.urls[dados.ix].url) = ", this.urls[dados.ix].url)
    this.autorizacao = this.autorizacoes[dados.ix].auth;
    this.loading = this.loader.create({
      content: "Conectando a reservatório: " + dados.nome + "...",
    });
    this.loading.present();
    this.getMDNS(dados.nome, this.urls[dados.ix].url);
  }
  //////////////////////////UNIFICAR => ProcurarReservatóriosPage//////////////////////

  watchMDNS() {
    this.n = 0;
    this.servicoMDNS = [];
    this.zeroconf.close();
    // this.tentarNovamente = false;
    let posicao = 0;
    this.loading = this.loader.create({
      content: "Procurando a Reservatório...",
    });

    // this.tentarNovamente = true;
    this.loading.present()
      .then(() => {
        ////////////////////////////TESTAR////////////////////////////
        this.timer = setTimeout(() => {
          this.storage.get("nomeReservatorio").then(nomeReservatorio => {
            this.storage.get("nomesReservatorios").then(nomesReservatorios => {
              console.log("recuperarEndPage nomeReservatorio = ", nomeReservatorio)
              console.log("recuperarEndPage this.servicoMDNS = ", this.servicoMDNS)
              let index00;
              let index;
              if (this.servicoMDNS.length > 0) {
                if (this.nomeOutra != null) {
                  index00 = nomesReservatorios.findIndex(i => i.alias === this.nomeOutra);//Procura o indice da reservatorio atual                  
                } else {
                  index00 = nomesReservatorios.findIndex(i => i.alias === nomeReservatorio);//Procura o indice da reservatorio atual
                }
                if (index00 >= 0) {
                  index = this.servicoMDNS.findIndex(i => i.name === nomesReservatorios[index00].name);//Verifica se o [index00].nome bate com a busca MDNS
                }
                if (index >= 0) {
                  console.log("recuperarEndPage this.servicoMDNS[" + index + "].name = ", this.servicoMDNS[index].name)
                  console.log("recuperarEndPage this.servicoMDNS[" + index + "].ipv4Addresses = ", this.servicoMDNS[index].ipv4Addresses)
                  if (String(this.servicoMDNS[index].ipv4Addresses) != '0.0.0.0') {
                    console.log("entrei em => if (String(result.service.ipv4Addresses) != '0.0.0.0' && (result.service.name === nomesReservatorios[index].name)) {}")
                    this.nome = this.servicoMDNS[index].name;
                    this.numIp = this.servicoMDNS[index].ipv4Addresses;
                    console.log("###this.servicoMDNS.name### = ", this.nome);
                    console.log("###this.servicoMDNS.ipv4Addresses### = ", this.numIp);
                    // this.n++;
                    // console.log("n = ", this.n);
                    // console.log("this.servicoMDNS.length = ", this.servicoMDNS.length);
                    // if (this.n >= this.servicoMDNS.length) {
                    if (this.numIp.length > 0) {
                      this.numIp = this.numIp[0];
                    }
                    this.value.editUrl(this.numIp);
                    this.getMDNS(this.nome, this.numIp);
                    // }
                  }
                } else {
                  this.loading.dismiss();
                  this.montarAlerta();
                }
              } else {
                this.loading.dismiss();
                this.montarAlerta();
              }
            });
          });
          this.zeroconf.stop();
          // this.toast.create({ message: "Nenhuma reservatório Encontrada!", duration: 2000, position: 'bottom' }).present();
          // console.log("Tentar Novamente: (dps do zeroconf unwatch) ", this.tentarNovamente);

          // this.zeroconf.reInit();
          // this.appthis.app.getRootNav().setRoot(RecarregarPage);
          console.log("Timeout: Nenhuma Reservatório encontrada ");
        }, 4000);
        ////////////////////////////////////////////////////////   

        this.zeroconf.watch('_coil-aurora._tcp.', 'local.')
          .subscribe(result => {
            console.log("Iniciando a busca por reservatórios... (recupararEndPage)")
            if (result.action == 'resolved') {
              console.log('service resolved', result.service);
              this.servicoMDNS.push(result.service);
              console.log("recuperarEndPage result.service.name = ", result.service.name);
              console.log("recuperarEndPage result.service.ipv4Addresses = ", result.service.ipv4Addresses);
            }
          })
      });
  }

  pararWatchMDNS() {
    this.zeroconf.unwatch('_http._tcp.', 'local.')
      .then(res => {
        console.log("unwatch() res: ", res);
      }).catch(err => {
        console.error("unwatch() err: ", err);
      });
    clearTimeout(this.timer);
  }

  getMDNS(nmdns, ipmdns) {
    let auth = new Date().toLocaleString();
    // this.pararWatchMDNS();//NOVO! TESTAR!
    // this.zeroconf.close();
    // this.loading = this.loader.create({
    //   content: "Configurando o APP Aurora...",
    // });
    // this.loading.present()
    //   .then(() => {
    this.procurarReservatorioProvider.getDadosReservatorioIP(ipmdns)//Tenta resolver por IP
      .then((resp) => {
        this.resposta = resp;
        console.log("GET Request Resolvendo IP Completo ", resp);
        this.autenticacaoProvider.getAutorizacaoJSON(ipmdns)//Busca a existência de uma chave no ESP
          .then(respauth => {
            console.log("Autorizacao IP: resp =", respauth);
            if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave...***Verificar Ncessidade*****
              this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                .then(res00 => {
                  this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                    .then(res01 => {
                      this.value.setUrl(ipmdns);//Altera o valor da variável url do ESP no APP
                      console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                      // this.autenticacaoProvider.setAutorizacaoJSON(auth, ipmdns)//Grava no ESP a chave de autenticação
                      //   .then(res02 => {
                      // console.log("Sucesso ao gravar autorização no ESP!", res02);
                      this.loading.dismiss();
                      console.log("carregarReservatorio(nomeReservatorio) ", nmdns)
                      this.carregarAPP(nmdns);
                      // }).catch(err02 => {
                      //   this.loading.dismiss();
                      //   console.error("Erro ao gravar autorização no ESP!", err02);
                      // })
                    }).catch(err01 => {
                      this.loading.dismiss();
                      console.error("Erro ao gravar a url values.provider: " + ipmdns + "=> ", err01)
                    });
                })
                .catch((e) => {
                  this.resposta = e;
                  console.error("ERRO Resolvendo IP!", e);
                  this.loading.dismiss();
                });
            } else if (respauth['autorizacao'] == this.autorizacao) {
              this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                .then(res01 => {
                  console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                  this.value.setUrl(ipmdns);
                  this.atualizarReservatorioSelecionada(ipmdns);
                  if (this.autorizacao.length > 0) {
                    this.autorizacao = this.autorizacao[0];
                  }
                  this.storage.set("auth", this.autorizacao).then(resAuth => {
                    console.log("Autorização atualizada", resAuth);
                  })
                  this.loading.dismiss();
                  console.log("carregarReservatorio(nomeReservatorio) ", nmdns)
                  this.carregarAPP(nmdns);
                });
            } else {
              console.log("Acesso NEGADO!");
              console.log("respauth['autorizacao']: ", respauth['autorizacao']);
              console.log("this.autorizacao ", this.autorizacao);
              this.storage.get("auth").then(resp => {
                this.autorizacao = resp;
                console.log("Autorização recuperada (Storage): ", resp);
              });
              this.loading.dismiss();
              this.alertaAcesoNegado(this.nomesReservatorios);
              // this.toast.create({ message: "Acesso Negado à esta reservatório!", duration: 5000, position: 'top' }).present();
              // this.navCtrl.push(AcessoNegadoPage);
            }
          }).catch(erroauth => {
            console.error("IP => Erro (POST) ao tentar autenticar...", erroauth);
            this.loading.dismiss();
            // this.navCtrl.push(RecarregarPage);
            this.alertaAcesoNegado(this.nomesReservatorios);
          })
      }).catch((e) => {
        console.error("ERRO Resolvendo IP!", e);
        if (this.nomeOutra != null) {
          this.watchMDNS();
          this.nomeOutra = null;//Verificar se a variável não ficará nula antes de percorrer o método watchMdns();
        } else {
          this.loading.dismiss();
          // this.navCtrl.push(RecarregarPage);       
          this.montarAlerta();
        }

      });
    // });
  }

  confirmacaoReset() {
    this.resetValoresProvider.confirmacaoReset();
  }

}
