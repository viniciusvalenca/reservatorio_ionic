import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Component } from '@angular/core';
import { NavController, LoadingController, Events } from 'ionic-angular';
import { HomeProvider } from '../../providers/home/home';
import { ValuesProvider } from '../../providers/values/values';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/timer';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  titulo: any;
  liquido: any = 0;
  percentual: any = 0;
  loading: any;
  bombaLigada: boolean;
  infoBomba: any;
  litrosMax: any;

  constructor(public navCtrl: NavController, public homeProvider: HomeProvider, public events: Events,
    public value: ValuesProvider, private screenOrientation: ScreenOrientation,
    private storage: Storage, public loader: LoadingController) {    
    this.titulo = "Reservatório";
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
      console.info("Orientação: Retrato.");
    });
    this.carregarConfiguracoes();
    this.eventoBombaLigada();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter - HOME PAGE');
  }

  eventoBombaLigada() {
    this.events.subscribe('bombaLigada', (bombaLigada) => {
      this.bombaLigada = bombaLigada;
      console.log("this.bombaLigada em Home.ts = ", bombaLigada);
      this.dadosBomba();
    });
  }

  carregarConfiguracoes() {
    this.storage.get("litro")
      .then(dados => {
        this.litrosMax = Math.round(dados);
        console.log("Storage GET litroVolume: ", dados);
      })
    this.storage.get("nome").then(dados => {
      this.titulo = dados;
      this.exibirNivel();
      console.log("Storage GET litroVolume: ", dados);
    })
  }

  exibirNivel() {
    this.loading = this.loader.create({
      content: "Obtendo informações de " + this.titulo + "...",
    });
    this.loading.present().then(() => {
      console.log("Timer INICIADO!");
      this.value.timer = setInterval(() => {
        this.carregarEstadoBomba();
        this.carregarNivel();
      }, 2000);
    })
  }

  carregarNivel() {
    this.homeProvider.getNivel()
      .subscribe(
      data => {
        //this.temperatura = JSON.stringify(data);  
        this.percentual = data;
        this.liquido = Number(data) * 2;
        // this.storage.set("liquido: ", this.liquido);
        console.log("Sucesso!");
        console.log(this.liquido);
        this.loading.dismiss();
      },
      err => {
        this.liquido = "FALHA...";
        console.log("Erro!");
        console.log(err);
        this.paginaIndisponivel();
        this.loading.dismiss();
      },
      () => console.log('GET Request completo'));
  }

  carregarEstadoBomba() {
    this.homeProvider.getBomba()
      .subscribe(
      data => {
        console.log("Resultado GET Estado Bomba", data);
        this.bombaLigada = Number(data) == 0 ? false : true;
        console.log("Estado Bomba (booblean): ", this.bombaLigada);
        this.dadosBomba();
      },
      err => {
        this.liquido = "FALHA...";
        console.log("Erro! ");
        console.log(err);
        this.paginaIndisponivel();
        this.loading.dismiss();
      },
      () => console.log('GET Request completo'));
  }

  dadosBomba() {
    if (this.bombaLigada) {
      this.infoBomba = "Bomba em funcionamento."
    } else {
      this.infoBomba = "Bomba desligada."
    }
  }

  ionViewDidLeave() {
    this.pararTimer();
  }

  pararTimer() {
    clearInterval(this.value.timer);
    console.log("Timer CANCELADO!");
  }

  paginaIndisponivel() {
    this.pararTimer();
    this.navCtrl.setRoot(IndisponivelPage);
  }
}