import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AjusteReservatorioProvider } from '../../providers/ajuste-reservatorio/ajuste-reservatorio';
import { ControleProvider } from '../../providers/controle/controle';
import { Slides } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-wizard',
  templateUrl: 'wizard.html',
})

export class WizardPage {

  @ViewChild(Slides) slides: Slides;

  litro: any;
  nome: any;
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private storage: Storage, private platform: Platform,
    public loader: LoadingController, private controleProvider: ControleProvider,
    private ajusteReservatorioPrvider: AjusteReservatorioProvider) {

    this.carregarAjustes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WizardPage');
  }

  carregarAjustes() {
    let loading = this.loader.create({
      content: "Carregando ajustes...",
    });
    loading.present().then(() => {
      this.storage.get("litro")
        .then(dados => {
          this.litro = dados;
          console.log("Storage GET litroVolume: ", dados);
          loading.dismiss();
        })
    })
  }

  gravarLitros() {
    let valor = {
      litros: Number(this.litro),
    }
    this.ajusteReservatorioPrvider.setLitroJSON(valor)
      .then(res => {
        console.log("Resposta SET Litros", res);
        this.storage.set("litro", this.litro)
          .then(dados => {
            console.log("Storage SET litro: ", dados);
            this.gravarNome();
          })
      }).catch(err => {
        console.error("Erro! ", err);
        this.loading.dismiss();
      })
  }

  gravarNome() {
    this.storage.set("nome", this.nome)
      .then(dados => {
        console.log("Storage SET Nome: ", dados);
        this.loading.dismiss();
        this.navCtrl.setRoot(TabsPage);
      }).catch(err => {
        console.error("Erro! ", err);
        this.loading.dismiss();
      })
  }

  pronto() {
    this.loading = this.loader.create({
      content: "Gravando preferências...",
    });
    this.loading.present().then(() => {
      this.gravarLitros();
    })
  }

  goToSlide(slide) {
    this.slides.slideTo(slide, 500);
  }
}
